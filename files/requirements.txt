# All the modules needed by OdooCoop and not found in the OCA/OCB core
openupgradelib
odoo12-addon-account-asset-management==12.0.2.3.0.99.dev3
odoo12-addon-account-banking-mandate==12.0.2.0.0.99.dev6
odoo12-addon-account-banking-pain-base==12.0.1.0.1
odoo12-addon-account-banking-sepa-credit-transfer==12.0.1.0.0.99.dev5
odoo12-addon-account-banking-sepa-direct-debit==12.0.1.0.0.99.dev15
odoo12-addon-account-due-list==12.0.1.0.0.99.dev7
odoo12-addon-account-financial-report==12.0.1.2.0
odoo12-addon-account-lock-date-update==12.0.1.0.0.99.dev13
odoo12-addon-account-payment-mode==12.0.1.0.0.99.dev12
odoo12-addon-account-payment-order==12.0.1.0.2
odoo12-addon-account-payment-partner==12.0.1.0.0.99.dev12
odoo12-addon-account-netting==12.0.1.0.0.99.dev13
odoo12-addon-base-bank-from-iban==12.0.1.0.0.99.dev2
odoo12-addon-base-technical-features==12.0.1.0.0.99.dev11
odoo12-addon-contract==12.0.8.3.8.99.dev3
odoo12-addon-contract-sale==12.0.3.0.0.99.dev1
odoo12-addon-contract-sale-invoicing==12.0.1.0.3
odoo12-addon-contract-variable-qty-timesheet==12.0.1.0.0.99.dev1
odoo12-addon-contract-variable-quantity==12.0.3.1.0.99.dev3
odoo12-addon-invoice-commercial-copy==12.0.0.0.1
odoo12-addon-knowledge==12.0.1.0.1.99.dev2
odoo12-addon-helpdesk-mgmt==12.0.1.23.1.99.dev4
odoo12-addon-helpdesk-mgmt-project==12.0.1.1.1
odoo12-addon-helpdesk-mgmt-timesheet==12.0.1.2.0.99.dev7
odoo12-addon-helpdesk-mgmt-timesheet-time-control==12.0.1.0.0.99.dev6
odoo12-addon-hr-holidays-public==12.0.1.1.1.99.dev5
odoo12-addon-l10n-es-account-asset==12.0.2.0.6.99.dev4
odoo12-addon-l10n-es-account-bank-statement-import-n43==12.0.1.0.3
odoo12-addon-l10n-es-account-invoice-sequence==12.0.1.0.1.99.dev5
odoo12-addon-l10n-es-aeat==12.0.2.3.0.99.dev5
odoo12-addon-l10n-es-aeat-mod111==12.0.1.4.0
odoo12-addon-l10n-es-aeat-mod115==12.0.1.4.0.99.dev3
odoo12-addon-l10n-es-aeat-mod123==12.0.1.3.0.99.dev2
odoo12-addon-l10n-es-aeat-mod190==12.0.1.0.2
odoo12-addon-l10n-es-aeat-mod303==12.0.3.0.0
odoo12-addon-l10n-es-aeat-mod347==12.0.2.0.0.99.dev2
odoo12-addon-l10n-es-aeat-mod349==12.0.1.3.3
odoo12-addon-l10n-es-aeat-mod390==12.0.3.1.0
odoo12-addon-l10n-es-mis-report==12.0.1.1.0
odoo12-addon-l10n-es-partner==12.0.1.0.2
odoo12-addon-l10n-es-toponyms==12.0.1.0.0.99.dev7
odoo12-addon-l10n-es-vat-book==12.0.1.5.0
odoo12-addon-mail-force-sender==12.0.1.0.5
odoo12-addon-mail-tracking-mass-mailing==12.0.1.0.0.99.dev5
odoo12-addon-mass-mailing-event-registration-exclude==12.0.1.0.0.99.dev3
odoo12-addon-mass-mailing-list-dynamic==12.0.1.0.4.99.dev1
odoo12-addon-mass-mailing-newsletter-welcome-mail==12.0.1.0.0.99.dev4
odoo12-addon-mass-mailing-unique==12.0.1.0.2.99.dev2
odoo12-addon-mail-force-return-path==12.0.1.0.1
odoo12-addon-mis-builder==12.0.3.7.1.99.dev3
odoo12-addon-mis-builder-budget==12.0.3.5.0.99.dev3
odoo12-addon-mis-builder-cash-flow==12.0.1.3.0.99.dev8
odoo12-addon-product-contract==12.0.5.2.1.99.dev3
odoo12-addon-product-contract-variable-quantity==12.0.1.0.1.99.dev2
odoo12-addon-project-status==12.0.1.1.1
odoo12-addon-project-task-default-stage==12.0.1.0.0.99.dev17
odoo12-addon-project-task-dependency==12.0.1.0.0.99.dev15
odoo12-addon-project-template==12.0.1.0.1
odoo12-addon-project-timeline==12.0.1.3.0
odoo12-addon-project-timeline-task-dependency==12.0.1.0.0.99.dev8
odoo12-addon-project-timesheet-time-control==12.0.2.1.0.99.dev16
odoo12-addon-project-tree-view==12.0.1.0.0.99.dev1
odoo12-addon-verbose-name-ticket==12.0.1.0.2
odoo12-addon-web-decimal-numpad-dot==12.0.1.0.0.99.dev9
odoo12-addon-web-favicon==12.0.1.0.0.99.dev8
odoo12-addon-web-no-bubble==12.0.1.0.0.99.dev4
odoo12-addon-web-responsive==12.0.1.3.0
odoo12-addon-web-searchbar-full-width==12.0.1.0.0.99.dev3
